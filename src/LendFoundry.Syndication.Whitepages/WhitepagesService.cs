﻿using LendFoundry.Syndication.Whitepages.Proxy;
using LendFoundry.Syndication.Whitepages.Request;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using System;
using System.Threading.Tasks;
using System.Linq;
using LendFoundry.Syndication.Whitepages.Events;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;

namespace LendFoundry.Syndication.Whitepages
{
    public class WhitepagesService : IWhitepagesService
    {
        #region Public Constructors

        public WhitepagesService(IWhitepagesProxy proxy, IEventHubClient eventHub, ILookupService lookup,ILogger logger,ITenantTime tenantTime)
        {
            if (proxy == null)
                throw new ArgumentNullException(nameof(proxy));
            if (eventHub == null)
                throw new ArgumentNullException(nameof(eventHub));
            if (lookup == null)
                throw new ArgumentNullException(nameof(lookup));
            Proxy = proxy;
            EventHub = eventHub;
            Lookup = lookup;
            Logger = logger;
            TenantTime = tenantTime;
        }

        #endregion Public Constructors

        #region Private Properties

        private IEventHubClient EventHub { get; }
        private ILookupService Lookup { get; }
        private IWhitepagesProxy Proxy { get; }

        private ILogger Logger { get; }
        private ITenantTime TenantTime { get; }

        #endregion Private Properties

        #region Public Methods

        /// <summary>
        /// Verifies the lead.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="leadRequest">The lead request.</param>
        /// <returns>Lead Verification Response.</returns>
        public async Task<Response.ILeadVerifyResponse> VerifyLead(string entityType, string entityId, ILeadVerifyRequest leadRequest)
        {
            entityType = EnsureEntityType(entityType);
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (leadRequest == null)
                throw new ArgumentNullException(nameof(leadRequest));
            //either "Name" or "firstName lastName" is required
            if (String.IsNullOrEmpty(leadRequest.Name) &
                (String.IsNullOrEmpty(leadRequest.FirstName)
                || String.IsNullOrEmpty(leadRequest.LastName)))
                throw new ArgumentNullException(nameof(leadRequest.Name) + " OR "
                    + nameof(leadRequest.FirstName) + " AND " + nameof(leadRequest.LastName));
            try
            {
                Logger.Info("Started Execution for VerifyLead Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Syndication: Whitepages");

                var proxyRequest = new Proxy.Request(leadRequest);
                var response = Proxy.VerifyLead(proxyRequest);
                var result = new Response.LeadVerifyResponse(response);
                await EventHub.Publish(new WhitePagesVerifyLeadRequested
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = result,
                    Request = leadRequest,
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });
                Logger.Info("Completed Execution for VerifyLead Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Syndication: Whitepages");

                return result;
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing VerifyLead Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + " TypeOf Syndication: Whitepages");

                await EventHub.Publish(new WhitePagesVerifyLeadRequestedFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.Message,
                    Request = leadRequest,
                    ReferenceNumber = null
            });
                throw ;
            }
        }

        public async Task<Response.IIdentityCheckResponse> IdentityCheck(string entityType, string entityId, Request.IIdentityCheckRequest identityCheckRequest)
        {
            entityType = EnsureEntityType(entityType);
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException(nameof(entityId));
            if (identityCheckRequest == null)
                throw new ArgumentNullException(nameof(identityCheckRequest));
            try
            {
                var proxyRequest = new Proxy.IdentityCheckRequest(identityCheckRequest);
                var response = Proxy.IdentityCheck(proxyRequest);
                var result = new Response.IdentityCheckResponse(response);
                await EventHub.Publish(new WhitePagesIdentityCheckRequested
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = result,
                    Request = identityCheckRequest,
                    ReferenceNumber = Guid.NewGuid().ToString("N")
                });
                return result;
            }
            catch (Exception exception)
            {
                await EventHub.Publish(new WhitePagesIdentityCheckRequestedFail
                {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.Message,
                    Request = identityCheckRequest,
                    ReferenceNumber = null
                });
                throw;
            }
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Ensures the type of the entity.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <returns>String entityType.</returns>
        private string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException(nameof(entityType));
            entityType = entityType.ToLower();
            var validEntityTypes = Lookup.GetLookupEntry("entityTypes", entityType);
            if (validEntityTypes == null || !validEntityTypes.Any())
                throw new InvalidArgumentException("Invalid Entity Type");
            return entityType;
        }

        #endregion Private Methods
    }
}