﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Whitepages
{
    public interface IWhitepagesConfiguration : IDependencyConfiguration
    {
        #region Public Properties

        string ApiBaseUrl { get; set; }
        string LeadVerifyApiKey { get; set; }
        string LeadVerifyApiUrl { get; set; }
        string IdentityCheckApiKey { get; set; }
        string IdentityCheckApiUrl { get; set; }
        string ConnectionString { get; set; }
        #endregion Public Properties
    }
}