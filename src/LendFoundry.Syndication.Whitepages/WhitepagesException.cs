﻿using System;
using System.Runtime.Serialization;

namespace LendFoundry.Syndication.Whitepages
{
    [Serializable]
    public class WhitepagesException : Exception
    {
       
        public string ErrorCode { get; set; }
        public WhitepagesException() { }
        public WhitepagesException(string message) : this(null, message, null)
        {

        }
        public WhitepagesException(string errorCode, string message) : this(errorCode, message, null)
        {

        }
        public WhitepagesException(string message, Exception innerException) : this(null, message, innerException)
        {

        }
        public WhitepagesException(string errorCode, string message, Exception innerException) : base(message, innerException)
        {
            ErrorCode = errorCode;
        }
        protected WhitepagesException(SerializationInfo info, StreamingContext context) : base(info, context)
        {

        }
    }
}
