﻿using LendFoundry.Syndication.Whitepages.Request;
using LendFoundry.Syndication.Whitepages.Response.IdentityCheck;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Whitepages.Response
{
    public class IdentityCheckResponse : IIdentityCheckResponse
    {
        #region Public Constructors

        public IdentityCheckResponse()
        {
        }

        public IdentityCheckResponse(Proxy.IIdentityCheckResponse identityCheckResponse)
        {
            if (identityCheckResponse != null)
            {
                Request = identityCheckResponse.Request != null ? new IdentityCheckRequest(identityCheckResponse.Request) : null;
                PrimaryPhoneChecks = identityCheckResponse.PrimaryPhoneChecks != null ? new PrimaryPhoneChecks(identityCheckResponse.PrimaryPhoneChecks) : null;
                SecondaryPhoneChecks = identityCheckResponse.SecondaryPhoneChecks != null ? new SecondaryPhoneChecks(identityCheckResponse.SecondaryPhoneChecks) : null;
                PrimaryAddressChecks = identityCheckResponse.PrimaryAddressChecks != null ? new PrimaryAddressChecks(identityCheckResponse.PrimaryAddressChecks) : null;
                SecondaryAddressChecks = identityCheckResponse.SecondaryAddressChecks != null ? new SecondaryAddressChecks(identityCheckResponse.SecondaryAddressChecks) : null;
                EmailAddressChecks = identityCheckResponse.EmailAddressChecks != null ? new IdentityCheck.EmailAddressChecks(identityCheckResponse.EmailAddressChecks) : null;
                IpAddressChecks = identityCheckResponse.IpAddressChecks != null ? new IdentityCheck.IPAddressChecks(identityCheckResponse.IpAddressChecks) : null;
                StolenIdentityCheck = identityCheckResponse.StolenIdentityCheck;
                IdentityCheckScore = identityCheckResponse.IdentityCheckScore;
                FraudEngineAttributes = identityCheckResponse.FraudEngineAttributes != null ? new FraudEngineAttributes(identityCheckResponse.FraudEngineAttributes) : null;
            }
        }

        #endregion Public Constructors

        #region Public Properties

        [JsonConverter(typeof(InterfaceConverter<IIdentityCheckRequest, IdentityCheckRequest>))]
        public IIdentityCheckRequest Request { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IPrimaryPhoneChecks, PrimaryPhoneChecks>))]
        public IPrimaryPhoneChecks PrimaryPhoneChecks { get; set; }
        [JsonConverter(typeof(InterfaceConverter<ISecondaryPhoneChecks, SecondaryPhoneChecks>))]
        public ISecondaryPhoneChecks SecondaryPhoneChecks { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPrimaryAddressChecks, PrimaryAddressChecks>))]
        public IPrimaryAddressChecks PrimaryAddressChecks { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ISecondaryAddressChecks, SecondaryAddressChecks>))]
        public ISecondaryAddressChecks SecondaryAddressChecks { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IdentityCheck.IEmailAddressChecks, IdentityCheck.EmailAddressChecks>))]
        public IdentityCheck.IEmailAddressChecks EmailAddressChecks { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IdentityCheck.IIPAddressChecks, IdentityCheck.IPAddressChecks>))]
        public IdentityCheck.IIPAddressChecks IpAddressChecks { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IFraudEngineAttributes, FraudEngineAttributes>))]
        public IFraudEngineAttributes FraudEngineAttributes { get; set; }
        public object StolenIdentityCheck{get;set;}
        public string IdentityCheckScore { get; set; }
        #endregion Public Properties
    }
}