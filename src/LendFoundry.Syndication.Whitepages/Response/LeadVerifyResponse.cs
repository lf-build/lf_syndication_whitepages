﻿using LendFoundry.Syndication.Whitepages.Request;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Whitepages.Response
{
    public class LeadVerifyResponse : ILeadVerifyResponse
    {
        #region Public Constructors

        public LeadVerifyResponse()
        {
        }

        public LeadVerifyResponse(Proxy.ILeadVerifyResponse leadVerifyResponse)
        {
            if (leadVerifyResponse != null)
            {
                Request = leadVerifyResponse.Request != null ? new LeadVerifyRequest(leadVerifyResponse.Request) : null;
                PhoneChecks = leadVerifyResponse.PhoneChecks != null ? new PhoneChecks(leadVerifyResponse.PhoneChecks) : null;
                AddressChecks = leadVerifyResponse.AddressChecks != null ? new AddressChecks(leadVerifyResponse.AddressChecks) : null;
                EmailAddressChecks = leadVerifyResponse.EmailAddressChecks != null ? new EmailAddressChecks(leadVerifyResponse.EmailAddressChecks) : null;
                IpAddressChecks = leadVerifyResponse.IpAddressChecks != null ? new IPAddressChecks(leadVerifyResponse.IpAddressChecks) : null;
            }
        }

        #endregion Public Constructors

        #region Public Properties

        [JsonConverter(typeof(InterfaceConverter<IAddressChecks, AddressChecks>))]
        public IAddressChecks AddressChecks { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IEmailAddressChecks, EmailAddressChecks>))]
        public IEmailAddressChecks EmailAddressChecks { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IIPAddressChecks, IPAddressChecks>))]
        public IIPAddressChecks IpAddressChecks { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPhoneChecks, PhoneChecks>))]
        public IPhoneChecks PhoneChecks { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ILeadVerifyRequest, LeadVerifyRequest>))]
        public ILeadVerifyRequest Request { get; set; }

        #endregion Public Properties
    }
}