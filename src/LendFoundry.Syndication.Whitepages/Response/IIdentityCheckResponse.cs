﻿using LendFoundry.Syndication.Whitepages.Request;
using LendFoundry.Syndication.Whitepages.Response.IdentityCheck;

namespace LendFoundry.Syndication.Whitepages.Response
{
    public interface IIdentityCheckResponse
    {
        #region Public Properties

        IIdentityCheckRequest Request { get; set; }
        IPrimaryPhoneChecks PrimaryPhoneChecks { get; set; }
        ISecondaryPhoneChecks SecondaryPhoneChecks { get; set; }
        IPrimaryAddressChecks PrimaryAddressChecks { get; set; }
        ISecondaryAddressChecks SecondaryAddressChecks { get; set; }
        IdentityCheck.IEmailAddressChecks EmailAddressChecks { get; set; }
        IdentityCheck.IIPAddressChecks IpAddressChecks { get; set; }
        IFraudEngineAttributes FraudEngineAttributes { get; set; }
        object StolenIdentityCheck { get; set; }
        string IdentityCheckScore { get; set; }
        #endregion Public Properties
    }
}