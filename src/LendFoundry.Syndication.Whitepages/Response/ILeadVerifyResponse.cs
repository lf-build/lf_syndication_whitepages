﻿using LendFoundry.Syndication.Whitepages.Request;

namespace LendFoundry.Syndication.Whitepages.Response
{
    public interface ILeadVerifyResponse
    {
        #region Public Properties

        IAddressChecks AddressChecks { get; set; }
        IEmailAddressChecks EmailAddressChecks { get; set; }
        IIPAddressChecks IpAddressChecks { get; set; }
        IPhoneChecks PhoneChecks { get; set; }
        ILeadVerifyRequest Request { get; set; }

        #endregion Public Properties
    }
}