﻿namespace LendFoundry.Syndication.Whitepages.Response
{
    public class AddressChecks : WhitepagesResponseBase, IAddressChecks
    {
        #region Public Constructors

        public AddressChecks()
        {
        }

        public AddressChecks(Proxy.IAddressChecks addressChecks) : base(addressChecks.Error, addressChecks.Warnings, addressChecks.IsValid, addressChecks.Diagnostics)
        {
            AddressContactScore = addressChecks.AddressContactScore;
            IsActive = addressChecks.IsActive;
            AddressToName = addressChecks.AddressToName;
            ResidentName = addressChecks.ResidentName;
            ResidentAgeRange = addressChecks.ResidentAgeRange;
            ResidentGender = addressChecks.ResidentGender;
            Type = addressChecks.Type;
            IsCommercial = addressChecks.IsCommercial;
            ResidentPhone = addressChecks.ResidentPhone;
        }

        #endregion Public Constructors

        #region Public Properties

        public int? AddressContactScore { get; set; }
        public string AddressToName { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsCommercial { get; set; }
        public string ResidentAgeRange { get; set; }
        public string ResidentGender { get; set; }
        public string ResidentName { get; set; }
        public string ResidentPhone { get; set; }
        public string Type { get; set; }

        #endregion Public Properties
    }
}