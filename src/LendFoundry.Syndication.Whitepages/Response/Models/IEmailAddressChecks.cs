﻿namespace LendFoundry.Syndication.Whitepages.Response
{
    public interface IEmailAddressChecks : IWhitepagesResponseBase
    {
        #region Public Properties

        int? EmailContactScore { get; set; }
        string EmailToName { get; set; }
        bool? IsDisposable { get; set; }
        string RegisteredName { get; set; }

        #endregion Public Properties
    }
}