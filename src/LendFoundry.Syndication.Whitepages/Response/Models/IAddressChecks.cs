﻿namespace LendFoundry.Syndication.Whitepages.Response
{
    public interface IAddressChecks : IWhitepagesResponseBase
    {
        #region Public Properties

        int? AddressContactScore { get; set; }
        string AddressToName { get; set; }
        bool? IsActive { get; set; }
        bool? IsCommercial { get; set; }
        string ResidentAgeRange { get; set; }
        string ResidentGender { get; set; }
        string ResidentName { get; set; }
        string ResidentPhone { get; set; }
        string Type { get; set; }

        #endregion Public Properties
    }
}