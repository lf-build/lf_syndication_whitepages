﻿namespace LendFoundry.Syndication.Whitepages.Response
{
    public class Geolocation : IGeolocation
    {
        #region Public Constructors

        public Geolocation()
        {
        }

        public Geolocation(Proxy.IGeolocation geolocation)
        {
            PostalNode = geolocation.PostalNode;
            CityName = geolocation.CityName;
            CountryName = geolocation.CountryName;
            ContinentCode = geolocation.ContinentCode;
            CountryCode = geolocation.CountryCode;
        }

        #endregion Public Constructors

        #region Public Properties

        public string CityName { get; set; }
        public string ContinentCode { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string PostalNode { get; set; }

        #endregion Public Properties
    }
}