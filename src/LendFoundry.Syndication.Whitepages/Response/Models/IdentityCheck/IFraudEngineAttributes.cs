﻿namespace LendFoundry.Syndication.Whitepages.Response.IdentityCheck
{
    public interface IFraudEngineAttributes
    {
        string BillingPhoneNorm { get; set; }
        string EmailAddressNorm { get; set; }
        string IpAddressNorm { get; set; }
        string PhoneNorm { get; set; }
        string ShippingPhoneNorm { get; set; }
    }
}