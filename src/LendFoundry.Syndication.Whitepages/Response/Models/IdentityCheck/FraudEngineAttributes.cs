﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Whitepages.Response.IdentityCheck
{
    public class FraudEngineAttributes : IFraudEngineAttributes
    {
        public FraudEngineAttributes(Proxy.IdentityCheck.IFraudEngineAttributes fraudEngineAttributes)
        {
            if (fraudEngineAttributes != null)
            {
                PhoneNorm = fraudEngineAttributes.PhoneNorm;
                BillingPhoneNorm = fraudEngineAttributes.BillingPhoneNorm;
                ShippingPhoneNorm = fraudEngineAttributes.ShippingPhoneNorm;
                EmailAddressNorm = fraudEngineAttributes.EmailAddressNorm;
                IpAddressNorm = fraudEngineAttributes.IpAddressNorm;
            }
        }
         public string PhoneNorm{ get; set; }
		 public string BillingPhoneNorm{ get; set; }
         public string ShippingPhoneNorm{ get; set; }
         public string EmailAddressNorm{ get; set; }
         public string IpAddressNorm{ get; set; }
    }
}
