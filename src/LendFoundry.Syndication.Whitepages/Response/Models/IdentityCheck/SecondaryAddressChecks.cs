﻿namespace LendFoundry.Syndication.Whitepages.Response.IdentityCheck
{
    public class SecondaryAddressChecks : WhitepagesResponseBase, ISecondaryAddressChecks
    {
        #region Public Constructors

        public SecondaryAddressChecks()
        {
        }

        public SecondaryAddressChecks(Proxy.IdentityCheck.ISecondaryAddressChecks addressChecks) : base(addressChecks.Error, addressChecks.Warnings, addressChecks.IsValid, addressChecks.Diagnostics)
        {
            if (addressChecks != null)
            {
                IsForwarder = addressChecks.IsForwarder;
                IsActive = addressChecks.IsActive;
                AddressToName = addressChecks.AddressToName;
                ResidentName = addressChecks.ResidentName;
                ResidentAgeRange = addressChecks.ResidentAgeRange;
                Type = addressChecks.Type;
                IsCommercial = addressChecks.IsCommercial;
                IsResidentDeceased = addressChecks.IsResidentDeceased;
            }
        }

        #endregion Public Constructors

        #region Public Properties

        public bool? IsForwarder { get; set; }
        public string AddressToName { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsCommercial { get; set; }
        public string ResidentAgeRange { get; set; }
        public bool? IsResidentDeceased { get; set; }
        public string ResidentName { get; set; }
        public string Type { get; set; }

        #endregion Public Properties
    }
}