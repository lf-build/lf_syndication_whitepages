﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Whitepages.Response.IdentityCheck
{
    public class SecondaryPhoneChecks : WhitepagesResponseBase, ISecondaryPhoneChecks
    {
        #region Public Constructors

        public SecondaryPhoneChecks()
        {
        }

        public SecondaryPhoneChecks(Proxy.IdentityCheck.ISecondaryPhoneChecks phoneChecks) : base(phoneChecks.Error, phoneChecks.Warnings, phoneChecks.IsValid, phoneChecks.Diagnostics)
        {
            if (phoneChecks != null)
            {
                PhoneContactScore = phoneChecks.PhoneContactScore;
                IsConnected = phoneChecks.IsConnected;
                PhoneToName = phoneChecks.PhoneToName;
                SubscriberName = phoneChecks.SubscriberName;
                SubscriberAgeRange = phoneChecks.SubscriberAgeRange;
                PhoneToAddress = phoneChecks.PhoneToAddress;
                CountryCode = phoneChecks.CountryCode;
                IsPrepaid = phoneChecks.IsPrepaid;
                LineType = phoneChecks.LineType;
                Carrier = phoneChecks.Carrier;
                IsCommercial = phoneChecks.IsCommercial;
                IsSubscriberDeceased = phoneChecks.IsSubscriberDeceased;
            }


        }

        #endregion Public Constructors

        #region Public Properties

        public string Carrier { get; set; }
        public string CountryCode { get; set; }
        public bool? IsCommercial { get; set; }
        public bool? IsConnected { get; set; }
        public object IsPrepaid { get; set; }
        public string LineType { get; set; }
        public int? PhoneContactScore { get; set; }
        public string PhoneToName { get; set; }
        public string PhoneToAddress { get; set; }
        public string SubscriberAgeRange { get; set; }
        public string SubscriberName { get; set; }
        public bool? IsSubscriberDeceased { get; set; }
        
        #endregion Public Properties
    }
}