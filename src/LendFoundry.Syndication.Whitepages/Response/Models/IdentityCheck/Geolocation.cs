﻿namespace LendFoundry.Syndication.Whitepages.Response.IdentityCheck
{
    public class Geolocation : IGeolocation
    {
        #region Public Constructors

        public Geolocation()
        {
        }

        public Geolocation(Proxy.IdentityCheck.IGeolocation geolocation)
        {
            PostalCode = geolocation.PostalCode;
            CityName = geolocation.CityName;
            CountryName = geolocation.CountryName;
            ContinentCode = geolocation.ContinentCode;
            CountryCode = geolocation.CountryCode;
            Subdivision = geolocation.Subdivision;
        }

        #endregion Public Constructors

        #region Public Properties

        public string CityName { get; set; }
        public string ContinentCode { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string PostalCode { get; set; }
        public string Subdivision { get; set; }

        #endregion Public Properties
    }
}