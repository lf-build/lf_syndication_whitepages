﻿namespace LendFoundry.Syndication.Whitepages.Response.IdentityCheck
{
    public interface IPrimaryPhoneChecks : IWhitepagesResponseBase
    {
        #region Public Properties

        string Carrier { get; set; }
        string CountryCode { get; set; }
        bool? IsCommercial { get; set; }
        bool? IsConnected { get; set; }
        object IsPrepaid { get; set; }
        string LineType { get; set; }
        int? PhoneContactScore { get; set; }
        string PhoneToName { get; set; }
        string SubscriberAgeRange { get; set; }
        string SubscriberName { get; set; }
        string PhoneToAddress { get; set; }
        bool? IsSubscriberDeceased { get; set; }

        #endregion Public Properties
    }
}