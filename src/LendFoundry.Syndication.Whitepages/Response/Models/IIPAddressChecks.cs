﻿namespace LendFoundry.Syndication.Whitepages.Response
{
    public interface IIPAddressChecks : IWhitepagesResponseBase
    {
        #region Public Properties

        string ConnectionType { get; set; }
        int? DistanceFromAddress { get; set; }
        int? DistanceFromPhone { get; set; }
        IGeolocation Geolocation { get; set; }
        bool? IsProxy { get; set; }

        #endregion Public Properties
    }
}