﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Whitepages.Response
{
    public interface IWhitepagesResponseBase
    {
        #region Public Properties

        IList<string> Diagnostics { get; set; }
        object Error { get; set; }
        bool? IsValid { get; set; }
        IList<string> Warnings { get; set; }

        #endregion Public Properties
    }
}