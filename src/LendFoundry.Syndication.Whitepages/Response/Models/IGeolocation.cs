﻿namespace LendFoundry.Syndication.Whitepages.Response
{
    public interface IGeolocation
    {
        #region Public Properties

        string CityName { get; set; }
        string ContinentCode { get; set; }
        string CountryCode { get; set; }
        string CountryName { get; set; }
        string PostalNode { get; set; }

        #endregion Public Properties
    }
}