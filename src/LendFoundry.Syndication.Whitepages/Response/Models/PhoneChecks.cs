﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Whitepages.Response
{
    public class PhoneChecks : WhitepagesResponseBase, IPhoneChecks
    {
        #region Public Constructors

        public PhoneChecks()
        {
        }

        public PhoneChecks(Proxy.IPhoneChecks phoneChecks) : base(phoneChecks.Error, phoneChecks.Warnings, phoneChecks.IsValid, phoneChecks.Diagnostics)
        {
            PhoneContactScore = phoneChecks.PhoneContactScore;
            IsConnected = phoneChecks.IsConnected;
            PhoneToName = phoneChecks.PhoneToName;
            SubscriberName = phoneChecks.SubscriberName;
            SubscriberAgeRange = phoneChecks.SubscriberAgeRange;
            SubscriberGender = phoneChecks.SubscriberGender;
            SubscriberAddress = new SubscriberAddress(phoneChecks.SubscriberAddress);
            CountryCode = phoneChecks.CountryCode;
            IsPrepaid = phoneChecks.IsPrepaid;
            LineType = phoneChecks.LineType;
            Carrier = phoneChecks.Carrier;
            IsCommercial = phoneChecks.IsCommercial;
        }

        #endregion Public Constructors

        #region Public Properties

        public string Carrier { get; set; }
        public string CountryCode { get; set; }
        public bool? IsCommercial { get; set; }
        public bool? IsConnected { get; set; }
        public object IsPrepaid { get; set; }
        public string LineType { get; set; }
        public int? PhoneContactScore { get; set; }
        public string PhoneToName { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ISubscriberAddress, SubscriberAddress>))]
        public ISubscriberAddress SubscriberAddress { get; set; }

        public string SubscriberAgeRange { get; set; }
        public string SubscriberGender { get; set; }
        public string SubscriberName { get; set; }

        #endregion Public Properties
    }
}