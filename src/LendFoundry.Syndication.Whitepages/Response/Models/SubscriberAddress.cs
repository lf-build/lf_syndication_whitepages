﻿namespace LendFoundry.Syndication.Whitepages.Response
{
    public class SubscriberAddress : ISubscriberAddress
    {
        #region Public Constructors

        public SubscriberAddress()
        {
        }

        public SubscriberAddress(Proxy.ISubscriberAddress subscriberAddress)
        {
            StreetLine1 = subscriberAddress.StreetLine1;
            StreetLine2 = subscriberAddress.StreetLine2;
            City = subscriberAddress.City;
            PostalCode = subscriberAddress.PostalCode;
            StateCode = subscriberAddress.StateCode;
            CountryName = subscriberAddress.CountryName;
            CountryCode = subscriberAddress.CountryCode;
        }

        #endregion Public Constructors

        #region Public Properties

        public string City { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string PostalCode { get; set; }
        public string StateCode { get; set; }
        public string StreetLine1 { get; set; }
        public string StreetLine2 { get; set; }

        #endregion Public Properties
    }
}