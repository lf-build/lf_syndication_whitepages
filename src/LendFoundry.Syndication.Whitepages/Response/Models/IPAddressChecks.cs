﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Whitepages.Response
{
    public class IPAddressChecks : WhitepagesResponseBase, IIPAddressChecks
    {
        #region Public Constructors

        public IPAddressChecks()
        {
        }

        public IPAddressChecks(Proxy.IIPAddressChecks ipAddressChecks) : base(ipAddressChecks.Error, ipAddressChecks.Warnings, ipAddressChecks.IsValid, ipAddressChecks.Diagnostics)
        {
            IsProxy = ipAddressChecks.IsProxy;
            Geolocation = new Geolocation(ipAddressChecks.Geolocation);
            DistanceFromAddress = ipAddressChecks.DistanceFromAddress;
            DistanceFromPhone = ipAddressChecks.DistanceFromPhone;
            ConnectionType = ipAddressChecks.ConnectionType;
        }

        #endregion Public Constructors

        #region Public Properties

        public string ConnectionType { get; set; }
        public int? DistanceFromAddress { get; set; }
        public int? DistanceFromPhone { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IGeolocation, Geolocation>))]
        public IGeolocation Geolocation { get; set; }

        public bool? IsProxy { get; set; }

        #endregion Public Properties
    }
}