﻿namespace LendFoundry.Syndication.Whitepages.Response
{
    public class EmailAddressChecks : WhitepagesResponseBase, IEmailAddressChecks
    {
        #region Public Constructors

        public EmailAddressChecks()
        {
        }

        public EmailAddressChecks(Proxy.IEmailAddressChecks emailAddressChecks) : base(emailAddressChecks.Error, emailAddressChecks.Warnings, emailAddressChecks.IsValid, emailAddressChecks.Diagnostics)
        {
            EmailContactScore = emailAddressChecks.EmailContactScore;
            IsDisposable = emailAddressChecks.IsDisposable;
            EmailToName = emailAddressChecks.EmailToName;
            RegisteredName = emailAddressChecks.RegisteredName;
        }

        #endregion Public Constructors

        #region Public Properties

        public int? EmailContactScore { get; set; }
        public string EmailToName { get; set; }
        public bool? IsDisposable { get; set; }
        public string RegisteredName { get; set; }

        #endregion Public Properties
    }
}