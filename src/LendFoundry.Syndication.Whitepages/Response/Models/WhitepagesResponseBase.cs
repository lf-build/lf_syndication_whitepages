﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Whitepages.Response
{
    public class WhitepagesResponseBase : IWhitepagesResponseBase
    {
        #region Public Constructors

        public WhitepagesResponseBase()
        {
        }

        public WhitepagesResponseBase(object error, IList<string> warnings, bool? isValid, IList<string> diagnostics)
        {
            Error = error;
            Warnings = warnings;
            IsValid = isValid;
            Diagnostics = diagnostics;
        }

        #endregion Public Constructors

        #region Public Properties

        public IList<string> Diagnostics { get; set; }
        public object Error { get; set; }
        public bool? IsValid { get; set; }
        public IList<string> Warnings { get; set; }

        #endregion Public Properties
    }
}