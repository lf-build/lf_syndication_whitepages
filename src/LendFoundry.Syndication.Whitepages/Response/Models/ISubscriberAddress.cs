﻿namespace LendFoundry.Syndication.Whitepages.Response
{
    public interface ISubscriberAddress
    {
        #region Public Properties

        string City { get; set; }
        string CountryCode { get; set; }
        string CountryName { get; set; }
        string PostalCode { get; set; }
        string StateCode { get; set; }
        string StreetLine1 { get; set; }
        string StreetLine2 { get; set; }

        #endregion Public Properties
    }
}