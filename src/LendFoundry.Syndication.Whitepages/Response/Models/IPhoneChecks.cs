﻿namespace LendFoundry.Syndication.Whitepages.Response
{
    public interface IPhoneChecks : IWhitepagesResponseBase
    {
        #region Public Properties

        string Carrier { get; set; }
        string CountryCode { get; set; }
        bool? IsCommercial { get; set; }
        bool? IsConnected { get; set; }
        object IsPrepaid { get; set; }
        string LineType { get; set; }
        int? PhoneContactScore { get; set; }
        string PhoneToName { get; set; }
        ISubscriberAddress SubscriberAddress { get; set; }
        string SubscriberAgeRange { get; set; }
        string SubscriberGender { get; set; }
        string SubscriberName { get; set; }

        #endregion Public Properties
    }
}