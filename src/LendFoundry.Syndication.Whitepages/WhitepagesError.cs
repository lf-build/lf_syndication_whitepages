﻿namespace LendFoundry.Syndication.Whitepages
{
    public enum WhitepagesErrorTypes
    {
        InvalidParametersError,
        InputFieldError,
        InputError,
        InternalError,
        AuthError,
        QuotaExceededError,
        ServerUnavailable
    }

    public class WhitepagesError
    {
        #region Public Properties

        public string Description { get; set; }
        public WhitepagesErrorTypes ErrorType { get; set; }
        public int HttpCode { get; set; }

        #endregion Public Properties
    }
}