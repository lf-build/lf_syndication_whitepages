﻿using LendFoundry.Syndication.Whitepages.Request;
using LendFoundry.Syndication.Whitepages.Response;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Whitepages
{
    public interface IWhitepagesService
    {
        #region Public Methods

        Task<ILeadVerifyResponse> VerifyLead(string entityType, string entityId, ILeadVerifyRequest leadRequest);

        Task<IIdentityCheckResponse> IdentityCheck(string entityType, string entityId, IIdentityCheckRequest identityCheckRequest);
        #endregion Public Methods
    }
}