﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Whitepages
{
    public class WhitepagesConfiguration : IWhitepagesConfiguration, IDependencyConfiguration
    {
        #region Public Properties

        public string ApiBaseUrl { get; set; }
        public string LeadVerifyApiKey { get; set; }
        public string LeadVerifyApiUrl { get; set; }
        public string IdentityCheckApiKey { get; set; }
        public string IdentityCheckApiUrl { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }

        #endregion Public Properties
    }
}