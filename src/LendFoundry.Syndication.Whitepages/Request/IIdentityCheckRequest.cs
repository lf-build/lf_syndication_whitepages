﻿namespace LendFoundry.Syndication.Whitepages.Request
{
    public interface IIdentityCheckRequest
    {
        #region Public Properties

        string ApiKey { get; set; }
        string Name { get; set; }
        string SecondaryName { get; set; }
        string PrimaryFirstname { get; set; }
        string PrimaryLastname { get; set; }
        string SecondaryFirstname { get; set; }
        string SecondaryLastname { get; set; }
        string PrimaryStreetLine_1 { get; set; }
        string PrimaryStreetLine_2 { get; set; }
        string PrimaryCity { get; set; }
        string PrimaryPostalCode { get; set; }
        string PrimaryStateCode { get; set; }
        string PrimaryCountryCode { get; set; }
        string SecondaryStreetLine_1 { get; set; }
        string SecondaryStreetLine_2 { get; set; }
        string SecondaryCity { get; set; }
        string SecondaryPostalCode { get; set; }
        string SecondaryStateCode { get; set; }
        string SecondaryCountryCode { get; set; }
        string PrimaryPhone { get; set; }
        string SecondaryPhone { get; set; }
        string EmailAddress { get; set; }
        string IpAddress { get; set; }
         string TransactionId { get; set; }
         string CustomerId { get; set; }

        #endregion Public Properties
    }
}