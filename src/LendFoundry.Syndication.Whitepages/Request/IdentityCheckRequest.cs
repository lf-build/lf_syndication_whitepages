﻿namespace LendFoundry.Syndication.Whitepages.Request
{
    public class IdentityCheckRequest : IIdentityCheckRequest
    {
        #region Public Constructors

        public IdentityCheckRequest()
        {
        }

        public IdentityCheckRequest(Proxy.IIdentityCheckRequest request)
        {
            Name = request.Name;
            SecondaryName = request.SecondaryName;
            PrimaryFirstname = request.PrimaryFirstname;
            PrimaryLastname = request.PrimaryLastname;
            SecondaryFirstname = request.SecondaryFirstname;
            SecondaryLastname = request.SecondaryLastname;
            PrimaryStreetLine_1 = request.PrimaryStreetLine_1;
            PrimaryStreetLine_2 = request.PrimaryStreetLine_2;
            PrimaryCity = request.PrimaryCity;
            PrimaryPostalCode = request.PrimaryPostalCode;
            PrimaryStateCode = request.PrimaryStateCode;
            PrimaryCountryCode = request.PrimaryCountryCode;
            SecondaryStreetLine_1 = request.SecondaryStreetLine_1;
            SecondaryStreetLine_2 = request.SecondaryStreetLine_2;
            SecondaryCity = request.SecondaryCity;
            SecondaryPostalCode = request.SecondaryPostalCode;
            SecondaryStateCode = request.SecondaryStateCode;
            SecondaryCountryCode = request.SecondaryCountryCode;
            PrimaryPhone = request.PrimaryPhone;
            SecondaryPhone = request.SecondaryPhone;
            EmailAddress = request.EmailAddress;
            IpAddress = request.IpAddress;
            TransactionId = request.TransactionId;
            CustomerId = request.CustomerId;
        }

        #endregion Public Constructors

        #region Public Properties

        public string Name { get; set; }
        public string SecondaryName { get; set; }
        public string PrimaryFirstname { get; set; }
        public string PrimaryLastname { get; set; }
        public string SecondaryFirstname { get; set; }
        public string SecondaryLastname { get; set; }
        public string PrimaryStreetLine_1 { get; set; }
        public string PrimaryStreetLine_2 { get; set; }
        public string PrimaryCity { get; set; }
        public string PrimaryPostalCode { get; set; }
        public string PrimaryStateCode { get; set; }
        public string PrimaryCountryCode { get; set; }
        public string SecondaryStreetLine_1 { get; set; }
        public string SecondaryStreetLine_2 { get; set; }
        public string SecondaryCity { get; set; }
        public string SecondaryPostalCode { get; set; }
        public string SecondaryStateCode { get; set; }
        public string SecondaryCountryCode { get; set; }
        public string PrimaryPhone { get; set; }
        public string SecondaryPhone { get; set; }
        public string EmailAddress { get; set; }
        public string IpAddress { get; set; }
        public string ApiKey { get; set; }
        public string TransactionId { get; set; }
        public string CustomerId { get; set; }
        #endregion Public Properties
    }
}