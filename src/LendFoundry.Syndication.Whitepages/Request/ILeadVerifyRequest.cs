﻿namespace LendFoundry.Syndication.Whitepages.Request
{
    public interface ILeadVerifyRequest
    {
        #region Public Properties

        string City { get; set; }
        string CountryCode { get; set; }
        string Email { get; set; }
        string FirstName { get; set; }
        string IpAddress { get; set; }
        string LastName { get; set; }
        string Name { get; set; }
        string Phone { get; set; }
        string PostalCode { get; set; }
        string StateCode { get; set; }
        string StreetLine1 { get; set; }
        string StreetLine2 { get; set; }

        #endregion Public Properties
    }
}