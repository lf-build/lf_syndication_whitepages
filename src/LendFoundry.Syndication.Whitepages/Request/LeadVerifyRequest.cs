﻿namespace LendFoundry.Syndication.Whitepages.Request
{
    public class LeadVerifyRequest : ILeadVerifyRequest
    {
        #region Public Constructors

        public LeadVerifyRequest()
        {
        }

        public LeadVerifyRequest(Proxy.IRequest request)
        {
            Name = request.Name;
            FirstName = request.FirstName;
            LastName = request.LastName;
            Phone = request.Phone;
            Email = request.Email;
            IpAddress = request.IpAddress;
            StreetLine1 = request.StreetLine1;
            StreetLine2 = request.StreetLine2;
            City = request.City;
            PostalCode = request.PostalCode;
            StateCode = request.StateCode;
            CountryCode = request.CountryCode;
        }

        #endregion Public Constructors

        #region Public Properties

        public string City { get; set; }
        public string CountryCode { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string IpAddress { get; set; }
        public string LastName { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string PostalCode { get; set; }
        public string StateCode { get; set; }
        public string StreetLine1 { get; set; }
        public string StreetLine2 { get; set; }

        #endregion Public Properties
    }
}