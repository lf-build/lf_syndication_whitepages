﻿using LendFoundry.SyndicationStore.Events;

namespace LendFoundry.Syndication.Whitepages.Events
{
    public class WhitePagesIdentityCheckRequested : SyndicationCalledEvent
    {
    }
}