﻿using LendFoundry.SyndicationStore.Events;

namespace LendFoundry.Syndication.Whitepages.Events
{
    public class WhitePagesIdentityCheckRequestedFail : SyndicationCalledEvent
    {
    }
}