﻿
namespace LendFoundry.Syndication.Whitepages.Proxy
{
    public interface IWhitepagesProxy
    {
        ILeadVerifyResponse VerifyLead(Request leadRequest);
        IIdentityCheckResponse IdentityCheck(IdentityCheckRequest leadRequest);
    }
}
