﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Net;
using System.Reflection;

namespace LendFoundry.Syndication.Whitepages.Proxy
{
    public class WhitepagesProxy : IWhitepagesProxy
    {
        #region Public Constructors

        public WhitepagesProxy(IWhitepagesConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            if (string.IsNullOrWhiteSpace(configuration.LeadVerifyApiKey))
                throw new ArgumentNullException(nameof(configuration.LeadVerifyApiKey));
            if (string.IsNullOrWhiteSpace(configuration.ApiBaseUrl))
                throw new ArgumentNullException(nameof(configuration.ApiBaseUrl));
            if (string.IsNullOrWhiteSpace(configuration.LeadVerifyApiUrl))
                throw new ArgumentNullException(nameof(configuration.LeadVerifyApiUrl));
            Configuration = configuration;
        }

        #endregion Public Constructors

        #region Private Properties

        private IWhitepagesConfiguration Configuration { get; }

        #endregion Private Properties

        #region Public Methods

        public ILeadVerifyResponse VerifyLead(Request leadRequest)
        {
            if (leadRequest == null)
                throw new ArgumentNullException(nameof(leadRequest));
            var client = new RestClient(Configuration.ApiBaseUrl.TrimEnd('/'));
            IRestRequest request = new RestRequest(Configuration.LeadVerifyApiUrl.TrimEnd('/'), Method.GET);
            AddParamToRequest(ref request, leadRequest);
            return ExecuteRequest<LeadVerifyResponse>(client, request,Configuration.LeadVerifyApiKey);
        }
        public IIdentityCheckResponse IdentityCheck(IdentityCheckRequest identityCheckRequest)
        {
            if (identityCheckRequest == null)
                throw new ArgumentNullException(nameof(identityCheckRequest));
            var client = new RestClient(Configuration.ApiBaseUrl.TrimEnd('/'));
            IRestRequest request = new RestRequest(Configuration.IdentityCheckApiUrl.TrimEnd('/'), Method.GET);
            AddParamToRequest(ref request, identityCheckRequest);
            return ExecuteRequest<IdentityCheckResponse>(client, request,Configuration.IdentityCheckApiKey);
        }
        #endregion Public Methods

        #region Private Methods

        private void AddParamToRequest<T>(ref IRestRequest request, T model, bool readJsonPropertyName = true) where T : class
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            PropertyInfo[] propertiesList = typeof(T).GetProperties();
            foreach (var prop in propertiesList)
            {
                if (prop.GetValue(model) != null)
                {
                    if (prop.PropertyType == typeof(int) && !prop.GetValue(model).Equals(0))
                    {
                        if (readJsonPropertyName)
                        {
                            request.AddParameter(prop.GetCustomAttribute<JsonPropertyAttribute>().PropertyName, prop.GetValue(model));
                        }
                        else
                        {
                            request.AddParameter(prop.Name, prop.GetValue(model));
                        }
                    }
                    if (prop.PropertyType == typeof(string) && !string.IsNullOrWhiteSpace(prop.GetValue(model).ToString()))
                    {
                        if (readJsonPropertyName)
                        {
                            request.AddParameter(prop.GetCustomAttribute<JsonPropertyAttribute>().PropertyName, prop.GetValue(model));
                        }
                        else
                        {
                            request.AddParameter(prop.Name, prop.GetValue(model));
                        }
                    }
                    if (prop.PropertyType == typeof(bool) && prop.GetValue(model).Equals(true))
                    {
                        if (readJsonPropertyName)
                        {
                            request.AddParameter(prop.GetCustomAttribute<JsonPropertyAttribute>().PropertyName, prop.GetValue(model));
                        }
                        else
                        {
                            request.AddParameter(prop.Name, prop.GetValue(model));
                        }
                    }
                }
            }
        }

        private T ExecuteRequest<T>(IRestClient client, IRestRequest request,string apiKey) where T : class
        {
            request.AddParameter("api_key", apiKey);
            var response = client.Execute(request);
            if (response == null)
                throw new ArgumentNullException(nameof(response));

            if (response.ErrorException != null)
                throw new WhitepagesException("Service call failed", response.ErrorException);

            if (response.ResponseStatus != ResponseStatus.Completed)
                throw new WhitepagesException(
                    $"Service call failed. Status {response.ResponseStatus}. Response: {response.ErrorMessage ?? ""}");

            if (response.StatusCode.ToString().ToLower() == "methodnotallowed")
                throw new WhitepagesException(
                    $"Service call failed. Status {response.ResponseStatus}. Response: {response.StatusDescription ?? ""}");
            if (response.StatusCode == HttpStatusCode.NotFound)
                throw new WhitepagesException(response.ErrorMessage);

            if (response.StatusCode == HttpStatusCode.NoContent)
                return null;

            if (response.StatusCode != HttpStatusCode.OK)
                throw new WhitepagesException(
                    $"Service call failed. Status {response.StatusCode}. Response: {response.ErrorMessage ?? ""}");
            try
            {
                return JsonConvert.DeserializeObject<T>(response.Content);
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to deserialize:" + response.ErrorMessage, exception);
            }
        }

        #endregion Private Methods
    }
}