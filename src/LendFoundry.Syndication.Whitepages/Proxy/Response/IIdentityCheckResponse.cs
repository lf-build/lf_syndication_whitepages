﻿using LendFoundry.Syndication.Whitepages.Proxy.IdentityCheck;

namespace LendFoundry.Syndication.Whitepages.Proxy
{
    public interface IIdentityCheckResponse
    {
         IIdentityCheckRequest Request { get; set; }

         IPrimaryPhoneChecks PrimaryPhoneChecks { get; set; }

         ISecondaryPhoneChecks SecondaryPhoneChecks { get; set; }

         IPrimaryAddressChecks PrimaryAddressChecks { get; set; }

         ISecondaryAddressChecks SecondaryAddressChecks { get; set; }

        IdentityCheck.IEmailAddressChecks EmailAddressChecks { get; set; }

        IdentityCheck.IIPAddressChecks IpAddressChecks { get; set; }

         object StolenIdentityCheck { get; set; }

         string IdentityCheckScore { get; set; }

         IFraudEngineAttributes FraudEngineAttributes { get; set; }
    }
}
