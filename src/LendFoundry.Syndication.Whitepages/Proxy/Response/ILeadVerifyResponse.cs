﻿namespace LendFoundry.Syndication.Whitepages.Proxy
{
    public interface ILeadVerifyResponse
    {
         IRequest Request { get; set; }
         IPhoneChecks PhoneChecks { get; set; }
         IAddressChecks AddressChecks { get; set; }
         IEmailAddressChecks EmailAddressChecks { get; set; }
         IIPAddressChecks IpAddressChecks { get; set; }
    }
}
