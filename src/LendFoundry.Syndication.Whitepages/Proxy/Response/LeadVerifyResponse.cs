﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Whitepages.Proxy
{
    public class LeadVerifyResponse: ILeadVerifyResponse
    {
        [JsonConverter(typeof(ConcreteJsonConverter<Request>))]
        [JsonProperty("request")]
        public IRequest Request { get; set; }

        [JsonConverter(typeof(ConcreteJsonConverter<PhoneChecks>))]
        [JsonProperty("phone_checks")]
        public IPhoneChecks PhoneChecks { get; set; }

        [JsonConverter(typeof(ConcreteJsonConverter<AddressChecks>))]
        [JsonProperty("address_checks")]
        public IAddressChecks AddressChecks { get; set; }

        [JsonConverter(typeof(ConcreteJsonConverter<EmailAddressChecks>))]
        [JsonProperty("email_address_checks")]
        public IEmailAddressChecks EmailAddressChecks { get; set; }

        [JsonConverter(typeof(ConcreteJsonConverter<IPAddressChecks>))]
        [JsonProperty("ip_address_checks")]
        public IIPAddressChecks IpAddressChecks { get; set; }
    }
}
