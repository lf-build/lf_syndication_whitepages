﻿using LendFoundry.Syndication.Whitepages.Proxy.IdentityCheck;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Whitepages.Proxy
{
    public class IdentityCheckResponse : IIdentityCheckResponse
    {
        [JsonConverter(typeof(ConcreteJsonConverter<IdentityCheckRequest>))]
        [JsonProperty("request")]
        public IIdentityCheckRequest Request { get; set; }

        [JsonConverter(typeof(ConcreteJsonConverter<PrimaryPhoneChecks>))]
        [JsonProperty("primary_phone_checks")]
        public IPrimaryPhoneChecks PrimaryPhoneChecks { get; set; }

        [JsonConverter(typeof(ConcreteJsonConverter<PrimaryPhoneChecks>))]
        [JsonProperty("secondary_phone_checks")]
        public ISecondaryPhoneChecks SecondaryPhoneChecks { get; set; }

        [JsonConverter(typeof(ConcreteJsonConverter<PrimaryAddressChecks>))]
        [JsonProperty("primary_address_checks")]
        public IPrimaryAddressChecks PrimaryAddressChecks { get; set; }

        [JsonConverter(typeof(ConcreteJsonConverter<PrimaryAddressChecks>))]
        [JsonProperty("secondary_address_checks")]
        public ISecondaryAddressChecks SecondaryAddressChecks { get; set; }

        [JsonConverter(typeof(ConcreteJsonConverter<IdentityCheck.EmailAddressChecks>))]
        [JsonProperty("email_address_checks")]
        public IdentityCheck.IEmailAddressChecks EmailAddressChecks { get; set; }

        [JsonConverter(typeof(ConcreteJsonConverter<IdentityCheck.IPAddressChecks>))]
        [JsonProperty("ip_address_checks")]
        public IdentityCheck.IIPAddressChecks IpAddressChecks { get; set; }

        [JsonProperty("stolen_identity_check")]
        public object StolenIdentityCheck { get; set; }

        [JsonProperty("identity_check_score")]
        public string IdentityCheckScore { get; set; }

        [JsonConverter(typeof(ConcreteJsonConverter<FraudEngineAttributes>))]
        [JsonProperty("fraud_engine_attributes")]
        public IFraudEngineAttributes FraudEngineAttributes { get; set; }
    }
}
