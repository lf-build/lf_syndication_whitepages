﻿using LendFoundry.Syndication.Whitepages.Request;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Whitepages.Proxy
{
    public class Request: IRequest
    {
        public Request()
        {

        }
        public Request(ILeadVerifyRequest request)
        {
            Name = request.Name;
            FirstName = request.FirstName;
            LastName = request.LastName;
            Phone = request.Phone;
            Email = request.Email;
            IpAddress = request.IpAddress;
            StreetLine1 = request.StreetLine1;
            StreetLine2 = request.StreetLine2;
            City = request.City;
            PostalCode = request.PostalCode;
            StateCode = request.StateCode;
            CountryCode = request.CountryCode;
        }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "firstname")]
        public string FirstName { get; set; }
        [JsonProperty(PropertyName = "lastname")]
        public string LastName { get; set; }
        [JsonProperty(PropertyName = "phone")]
        public string Phone { get; set; }
        [JsonProperty(PropertyName = "email_address")]
        public string Email { get; set; }
        [JsonProperty(PropertyName = "ip_address")]
        public string IpAddress { get; set; }
        [JsonProperty(PropertyName = "address.street_line_1")]
        public string StreetLine1 { get; set; }
        [JsonProperty(PropertyName = "address.street_line_2")]
        public string StreetLine2 { get; set; }
        [JsonProperty(PropertyName = "address.city")]
        public string City { get; set; }
        [JsonProperty(PropertyName = "address.postal_code")]
        public string PostalCode { get; set; }
        [JsonProperty(PropertyName = "address.state_code")]
        public string StateCode { get; set; }
        [JsonProperty(PropertyName = "address.country_code")]
        public string CountryCode { get; set; }
        [JsonProperty("api_key")]
        public string ApiKey { get; set; }
    }
}
