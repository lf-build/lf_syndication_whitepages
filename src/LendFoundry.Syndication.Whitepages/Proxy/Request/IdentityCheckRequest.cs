﻿using LendFoundry.Syndication.Whitepages.Request;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Whitepages.Proxy
{
    public class IdentityCheckRequest : IIdentityCheckRequest
    {
        public IdentityCheckRequest()
        {

        }
        public IdentityCheckRequest(LendFoundry.Syndication.Whitepages.Request.IIdentityCheckRequest request)
        {
            Name = request.Name;
            SecondaryName = request.SecondaryName;
            PrimaryFirstname = request.PrimaryFirstname;
            PrimaryLastname = request.PrimaryLastname;
            SecondaryFirstname = request.SecondaryFirstname;
            SecondaryLastname = request.SecondaryLastname;
            PrimaryStreetLine_1 = request.PrimaryStreetLine_1;
            PrimaryStreetLine_2 = request.PrimaryStreetLine_2;
            PrimaryCity = request.PrimaryCity;
            PrimaryPostalCode = request.PrimaryPostalCode;
            PrimaryStateCode = request.PrimaryStateCode;
            PrimaryCountryCode = request.PrimaryCountryCode;
            SecondaryStreetLine_1 = request.SecondaryStreetLine_1;
            SecondaryStreetLine_2 = request.SecondaryStreetLine_2;
            SecondaryCity = request.SecondaryCity;
            SecondaryPostalCode = request.SecondaryPostalCode;
            SecondaryStateCode = request.SecondaryStateCode;
            SecondaryCountryCode = request.SecondaryCountryCode;
            PrimaryPhone = request.PrimaryPhone;
            SecondaryPhone = request.SecondaryPhone;
            EmailAddress = request.EmailAddress;
            IpAddress = request.IpAddress;
            TransactionId = request.TransactionId;
            CustomerId = request.CustomerId;
        }

        [JsonProperty(PropertyName = "primary.name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "secondary.name")]
        public string SecondaryName { get; set; }
        [JsonProperty(PropertyName = "primary.firstname")]
        public string PrimaryFirstname { get; set; }
        [JsonProperty(PropertyName = "primary.lastname")]
        public string PrimaryLastname { get; set; }
        [JsonProperty(PropertyName = "secondary.firstname")]
        public string SecondaryFirstname { get; set; }
        [JsonProperty(PropertyName = "secondary.lastname")]
        public string SecondaryLastname { get; set; }
        [JsonProperty(PropertyName = "primary.address.street_line_1")]
         public string PrimaryStreetLine_1 { get; set; }
        [JsonProperty(PropertyName = "primary.address.street_line_2")]
        public string PrimaryStreetLine_2 { get; set; }
        [JsonProperty(PropertyName = "primary.address.city")]
        public string PrimaryCity { get; set; }
        [JsonProperty(PropertyName = "primary.address.postal_code")]
        public string PrimaryPostalCode { get; set; }
        [JsonProperty(PropertyName = "primary.address.state_code")]
        public string PrimaryStateCode { get; set; }
        [JsonProperty(PropertyName = "primary.address.country_code")]
        public string PrimaryCountryCode { get; set; }
        [JsonProperty(PropertyName = "secondary.address.street_line_1")]
        public string SecondaryStreetLine_1 { get; set; }
        [JsonProperty(PropertyName = "secondary.address.street_line_2")]
        public string SecondaryStreetLine_2 { get; set; }
        [JsonProperty(PropertyName = "secondary.address.city")]
        public string SecondaryCity { get; set; }
        [JsonProperty(PropertyName = "secondary.address.postal_code")]
        public string SecondaryPostalCode { get; set; }
        [JsonProperty(PropertyName = "secondary.address.state_code")]
        public string SecondaryStateCode { get; set; }
        [JsonProperty(PropertyName = "secondary.address.country_code")]
        public string SecondaryCountryCode { get; set; }
        [JsonProperty(PropertyName = "primary.phone")]
        public string PrimaryPhone { get; set; }
        [JsonProperty(PropertyName = "secondary.phone")]
        public string SecondaryPhone { get; set; }
        [JsonProperty(PropertyName = "email_address")]
        public string EmailAddress { get; set; }
        [JsonProperty(PropertyName = "ip_address")]
        public string IpAddress { get; set; }
        [JsonProperty(PropertyName = "transaction_id")]
        public string TransactionId { get; set; }
        [JsonProperty(PropertyName = "customer_id")]
        public string CustomerId { get; set; }
        [JsonProperty("api_key")]
        public string ApiKey { get; set; }
      
    }
}
