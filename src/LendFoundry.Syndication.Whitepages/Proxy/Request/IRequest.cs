﻿namespace LendFoundry.Syndication.Whitepages.Proxy
{
    public interface IRequest
    {
        string Name { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string Phone { get; set; }
        string Email { get; set; }
        string IpAddress { get; set; }
        string StreetLine1 { get; set; }
        string StreetLine2 { get; set; }
        string City { get; set; }
        string PostalCode { get; set; }
        string StateCode { get; set; }
        string CountryCode { get; set; }
        string ApiKey { get; set; }
    }
}
