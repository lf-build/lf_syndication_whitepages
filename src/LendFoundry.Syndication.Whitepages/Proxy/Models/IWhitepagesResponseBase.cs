﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Whitepages.Proxy
{
    public interface IWhitepagesResponseBase
    {
        object Error { get; set; }
        IList<string> Warnings { get; set; }
        bool? IsValid { get; set; }
        IList<string> Diagnostics { get; set; }
    }
}
