﻿namespace LendFoundry.Syndication.Whitepages.Proxy
{
    public interface IAddressChecks: IWhitepagesResponseBase
    {
        int? AddressContactScore { get; set; }
        bool? IsActive { get; set; }
        string AddressToName { get; set; }
        string ResidentName { get; set; }
        string ResidentAgeRange { get; set; }
        string ResidentGender { get; set; }
        string Type { get; set; }
        bool? IsCommercial { get; set; }
        string ResidentPhone { get; set; }
    }
}
