﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Whitepages.Proxy
{
    public class AddressChecks : WhitepagesResponseBase, IAddressChecks
    {
        #region Public Properties

        [JsonProperty("address_contact_score")]
        public int? AddressContactScore { get; set; }

        [JsonProperty("address_to_name")]
        public string AddressToName { get; set; }

        [JsonProperty("is_active")]
        public bool? IsActive { get; set; }

        [JsonProperty("is_commercial")]
        public bool? IsCommercial { get; set; }

        [JsonProperty("resident_age_range")]
        public string ResidentAgeRange { get; set; }

        [JsonProperty("resident_gender")]
        public string ResidentGender { get; set; }

        [JsonProperty("resident_name")]
        public string ResidentName { get; set; }

        [JsonProperty("resident_phone")]
        public string ResidentPhone { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        #endregion Public Properties
    }
}