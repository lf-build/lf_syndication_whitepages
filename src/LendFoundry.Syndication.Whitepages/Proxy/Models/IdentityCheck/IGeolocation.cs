﻿namespace LendFoundry.Syndication.Whitepages.Proxy.IdentityCheck
{
    public interface IGeolocation
    {
        #region Public Properties

        string CityName { get; set; }
        string ContinentCode { get; set; }
        string CountryCode { get; set; }
        string CountryName { get; set; }
        string PostalCode { get; set; }
        string Subdivision { get; set; }
        #endregion Public Properties
    }
}