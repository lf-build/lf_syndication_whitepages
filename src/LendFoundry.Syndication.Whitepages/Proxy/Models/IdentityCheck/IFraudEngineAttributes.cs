﻿namespace LendFoundry.Syndication.Whitepages.Proxy.IdentityCheck
{
    public interface IFraudEngineAttributes
    {
        string BillingPhoneNorm { get; set; }
        string EmailAddressNorm { get; set; }
        string IpAddressNorm { get; set; }
        string PhoneNorm { get; set; }
        string ShippingPhoneNorm { get; set; }
    }
}