﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Whitepages.Proxy.IdentityCheck
{
    public class FraudEngineAttributes: IFraudEngineAttributes
    {
      
        [JsonProperty("phone_norm")]
        public string PhoneNorm{ get; set; }
        [JsonProperty("billing_phone_norm")]
        public string BillingPhoneNorm{ get; set; }
        [JsonProperty("shipping_phone_norm")]
        public string ShippingPhoneNorm{ get; set; }
        [JsonProperty("email_address_norm")]
        public string EmailAddressNorm{ get; set; }
        [JsonProperty("ip_address_norm")]
        public string IpAddressNorm{ get; set; }
    }
}
