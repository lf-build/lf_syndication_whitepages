﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Whitepages.Proxy.IdentityCheck
{
    public class IPAddressChecks : WhitepagesResponseBase, IIPAddressChecks
    {


        #region Public Properties

        [JsonProperty("is_proxy")]
        public bool? IsProxy { get; set; }

        [JsonConverter(typeof(ConcreteJsonConverter<Geolocation>))]
        [JsonProperty("geolocation")]
        public IGeolocation Geolocation { get; set; }

        [JsonProperty("distance_from_address")]
        public int? DistanceFromAddress { get; set; }

        [JsonProperty("distance_from_phone")]
        public int? DistanceFromPhone { get; set; }

        [JsonProperty("connection_type")]
        public string ConnectionType { get; set; }
        #endregion Public Properties
    }
}