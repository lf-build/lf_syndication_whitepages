﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Whitepages.Proxy.IdentityCheck
{
    public class SecondaryAddressChecks : WhitepagesResponseBase, ISecondaryAddressChecks
    {


        #region Public Properties

        [JsonProperty("is_forwarder")]
        public bool? IsForwarder { get; set; }
        [JsonProperty("address_to_name")]
        public string AddressToName { get; set; }
        [JsonProperty("is_active")]
        public bool? IsActive { get; set; }
        [JsonProperty("is_commercial")]
        public bool? IsCommercial { get; set; }
        [JsonProperty("resident_age_range")]
        public string ResidentAgeRange { get; set; }
        [JsonProperty("is_resident_deceased")]
        public bool? IsResidentDeceased { get; set; }
        [JsonProperty("resident_name")]
        public string ResidentName { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }

        #endregion Public Properties
    }
}