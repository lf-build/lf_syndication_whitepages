﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Whitepages.Proxy.IdentityCheck
{
    public class SecondaryPhoneChecks : WhitepagesResponseBase, ISecondaryPhoneChecks
    {


        #region Public Properties

        [JsonProperty("carrier")]
        public string Carrier { get; set; }
        [JsonProperty("country_code")]
        public string CountryCode { get; set; }
        [JsonProperty("is_commercial")]
        public bool? IsCommercial { get; set; }
        [JsonProperty("is_connected")]
        public bool? IsConnected { get; set; }
        [JsonProperty("is_prepaid")]
        public object IsPrepaid { get; set; }
        [JsonProperty("line_type")]
        public string LineType { get; set; }
        [JsonProperty("phone_contact_score")]
        public int? PhoneContactScore { get; set; }
        [JsonProperty("phone_to_name")]
        public string PhoneToName { get; set; }
        [JsonProperty("phone_to_address")]
        public string PhoneToAddress { get; set; }
        [JsonProperty("subscriber_age_range")]
        public string SubscriberAgeRange { get; set; }
        [JsonProperty("subscriber_name")]
        public string SubscriberName { get; set; }
        [JsonProperty("is_subscriber_deceased")]
        public bool? IsSubscriberDeceased { get; set; }

        #endregion Public Properties
    }
}