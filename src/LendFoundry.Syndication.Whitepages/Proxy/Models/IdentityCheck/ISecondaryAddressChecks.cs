﻿namespace LendFoundry.Syndication.Whitepages.Proxy.IdentityCheck
{
    public interface ISecondaryAddressChecks : IWhitepagesResponseBase
    {
        #region Public Properties

        bool? IsForwarder { get; set; }
        string AddressToName { get; set; }
        bool? IsActive { get; set; }
        bool? IsCommercial { get; set; }
        string ResidentAgeRange { get; set; }
        bool? IsResidentDeceased { get; set; }
        string ResidentName { get; set; }
        string Type { get; set; }

        #endregion Public Properties
    }
}