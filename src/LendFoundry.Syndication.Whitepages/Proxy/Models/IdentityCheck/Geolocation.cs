﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Whitepages.Proxy.IdentityCheck
{
    public class Geolocation : IGeolocation
    {


        #region Public Properties

        [JsonProperty("postal_code")]
        public string PostalCode { get; set; }

        [JsonProperty("city_name")]
        public string CityName { get; set; }

        [JsonProperty("country_name")]
        public string CountryName { get; set; }

        [JsonProperty("continent_code")]
        public string ContinentCode { get; set; }

        [JsonProperty("country_code")]
        public string CountryCode { get; set; }

        [JsonProperty("subdivision")]
        public string Subdivision { get; set; }
        

        #endregion Public Properties
    }
}