﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Whitepages.Proxy
{
    public class WhitepagesResponseBase: IWhitepagesResponseBase
    {
        [JsonProperty("error")]
        public object Error { get; set; }

        [JsonProperty("warnings")]
        public IList<string> Warnings { get; set; }

        [JsonProperty("is_valid")]
        public bool? IsValid { get; set; }

        [JsonProperty("diagnostics")]
        public IList<string> Diagnostics { get; set; }
    }
}
