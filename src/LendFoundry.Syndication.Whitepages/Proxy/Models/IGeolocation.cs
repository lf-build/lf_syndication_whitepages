﻿namespace LendFoundry.Syndication.Whitepages.Proxy
{
    public interface IGeolocation
    {
        string PostalNode { get; set; }
        string CityName { get; set; }
        string CountryName { get; set; }
        string ContinentCode { get; set; }
        string CountryCode { get; set; }
    }
}
