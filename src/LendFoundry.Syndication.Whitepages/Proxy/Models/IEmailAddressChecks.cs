﻿namespace LendFoundry.Syndication.Whitepages.Proxy
{
    public interface IEmailAddressChecks : IWhitepagesResponseBase
    {
        int? EmailContactScore { get; set; }
        bool? IsDisposable { get; set; }
        string EmailToName { get; set; }
        string RegisteredName { get; set; }
    }
}
