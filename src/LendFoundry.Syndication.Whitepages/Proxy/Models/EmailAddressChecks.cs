﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Whitepages.Proxy
{
    public class EmailAddressChecks:WhitepagesResponseBase, IEmailAddressChecks
    {
        [JsonProperty("email_contact_score")]
        public int? EmailContactScore { get; set; }

        [JsonProperty("is_disposable")]
        public bool? IsDisposable { get; set; }

        [JsonProperty("email_to_name")]
        public string EmailToName { get; set; }

        [JsonProperty("registered_name")]
        public string RegisteredName { get; set; }
    }
}
