﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Whitepages.Proxy
{
    public class PhoneChecks : WhitepagesResponseBase, IPhoneChecks
    {
        [JsonProperty("phone_contact_score")]
        public int? PhoneContactScore { get; set; }
        [JsonProperty("is_connected")]
        public bool? IsConnected { get; set; }

        [JsonProperty("phone_to_name")]
        public string PhoneToName { get; set; }

        [JsonProperty("subscriber_name")]
        public string SubscriberName { get; set; }

        [JsonProperty("subscriber_age_range")]
        public string SubscriberAgeRange { get; set; }

        [JsonProperty("subscriber_gender")]
        public string SubscriberGender { get; set; }

        [JsonConverter(typeof(ConcreteJsonConverter<SubscriberAddress>))]
        [JsonProperty("subscriber_address")]
        public ISubscriberAddress SubscriberAddress { get; set; }

        [JsonProperty("country_code")]
        public string CountryCode { get; set; }

        [JsonProperty("is_prepaid")]
        public object IsPrepaid { get; set; }

        [JsonProperty("line_type")]
        public string LineType { get; set; }

        [JsonProperty("carrier")]
        public string Carrier { get; set; }

        [JsonProperty("is_commercial")]
        public bool? IsCommercial { get; set; }
    }
}
