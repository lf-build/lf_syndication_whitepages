﻿namespace LendFoundry.Syndication.Whitepages.Proxy
{
    public interface IPhoneChecks: IWhitepagesResponseBase
    {
        int? PhoneContactScore { get; set; }
        bool? IsConnected { get; set; }
        string PhoneToName { get; set; }
        string SubscriberName { get; set; }
        string SubscriberAgeRange { get; set; }
        string SubscriberGender { get; set; }
        ISubscriberAddress SubscriberAddress { get; set; }
        string CountryCode { get; set; }
        object IsPrepaid { get; set; }
        string LineType { get; set; }
        string Carrier { get; set; }
        bool? IsCommercial { get; set; }
    }
}
