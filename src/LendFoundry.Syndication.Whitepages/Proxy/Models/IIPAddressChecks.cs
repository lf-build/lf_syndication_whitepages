﻿namespace LendFoundry.Syndication.Whitepages.Proxy
{
    public interface IIPAddressChecks: IWhitepagesResponseBase
    {
       bool? IsProxy { get; set; }
       IGeolocation Geolocation { get; set; }
       int? DistanceFromAddress { get; set; }
       int? DistanceFromPhone { get; set; }
       string ConnectionType { get; set; }
    }
}
