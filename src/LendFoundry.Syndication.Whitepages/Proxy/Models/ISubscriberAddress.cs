﻿namespace LendFoundry.Syndication.Whitepages.Proxy
{
    public interface ISubscriberAddress
    {
        string StreetLine1 { get; set; }
        string StreetLine2 { get; set; }
        string City { get; set; }
        string PostalCode { get; set; }
        string StateCode { get; set; }
        string CountryName { get; set; }
        string CountryCode { get; set; }
    }
}
