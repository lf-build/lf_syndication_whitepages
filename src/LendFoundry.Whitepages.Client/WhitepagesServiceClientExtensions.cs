﻿using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using System;

using Microsoft.Extensions.DependencyInjection;
namespace LendFoundry.Whitepages.Client
{
    public static class WhitepagesServiceClientExtensions
    {
        #region Public Methods

        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddWhitepagesService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IWhitepagesServiceClientFactory>(p => new WhitepagesServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IWhitepagesServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
        public static IServiceCollection AddWhitepagesService(this IServiceCollection services, Uri uri)
        {
            services.AddSingleton<IWhitepagesServiceClientFactory>(p => new WhitepagesServiceClientFactory(p, uri));
            services.AddSingleton(p => p.GetService<IWhitepagesServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddWhitepagesService(this IServiceCollection services)
        {
            services.AddSingleton<IWhitepagesServiceClientFactory>(p => new WhitepagesServiceClientFactory(p));
            services.AddSingleton(p => p.GetService<IWhitepagesServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
        #endregion Public Methods
    }
}
