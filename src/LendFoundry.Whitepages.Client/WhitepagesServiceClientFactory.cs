﻿using System;
using LendFoundry.Security.Tokens;
using LendFoundry.Syndication.Whitepages;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Client;
using Microsoft.Extensions.DependencyInjection;

namespace LendFoundry.Whitepages.Client
{
    public class WhitepagesServiceClientFactory : IWhitepagesServiceClientFactory
    {
        #region Public Constructors

        [Obsolete("Need to use the overloaded with Uri")]
        public WhitepagesServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
            Uri = new UriBuilder("http", Endpoint, Port).Uri;
        }
        public WhitepagesServiceClientFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }
        #endregion Public Constructors

        #region Private Properties

        private string Endpoint { get; }
        private int Port { get; }
        private IServiceProvider Provider { get; }
        private Uri Uri { get; }

        #endregion Private Properties

        #region Public Methods

        public IWhitepagesService Create(ITokenReader reader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("whitepages-pro");
            }
            var client = Provider.GetServiceClient(reader, uri);
            return new WhitepagesService(client);
        }

        #endregion Public Methods
    }
}
