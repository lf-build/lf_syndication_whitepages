﻿using LendFoundry.Syndication.Whitepages;
using LendFoundry.Security.Tokens;

namespace LendFoundry.Whitepages.Client
{
    public interface IWhitepagesServiceClientFactory
    {
        IWhitepagesService Create(ITokenReader reader);
    }
}