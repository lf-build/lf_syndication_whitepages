﻿using System.Threading.Tasks;
using LendFoundry.Foundation.Client;
using RestSharp;
using LendFoundry.Syndication.Whitepages;
using LendFoundry.Syndication.Whitepages.Request;
using LendFoundry.Syndication.Whitepages.Response;
using System;

namespace LendFoundry.Whitepages.Client
{
    public class WhitepagesService : IWhitepagesService
    {
        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the WhitepagesService class.
        /// </summary>
        /// <param name="client">The client.</param>
        public WhitepagesService(IServiceClient client)
        {
            Client = client;
        }

        #endregion Public Constructors

        #region Private Properties

        /// <summary>
        /// Gets the client.
        /// </summary>
        /// <value>The client.</value>
        private IServiceClient Client { get; }


        #endregion Private Properties

        #region Public Method

        /// <summary>
        /// Verifies the lead.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="leadRequest">The lead request.</param>
        /// <returns>LeadVerification Response.</returns>
        public async Task<ILeadVerifyResponse> VerifyLead(string entityType, string entityId, ILeadVerifyRequest leadRequest)
        {
            var request = new RestRequest("{entitytype}/{entityid}/whitepages/verify_lead", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            request.AddJsonBody(leadRequest);
            return await Client.ExecuteAsync<LeadVerifyResponse>(request);
        }


        public async Task<IIdentityCheckResponse> IdentityCheck(string entityType, string entityId, IIdentityCheckRequest leadRequest)
        {
            var request = new RestRequest("{entitytype}/{entityid}/whitepages/identity_Check", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            request.AddJsonBody(leadRequest);
            return await Client.ExecuteAsync<IdentityCheckResponse>(request);
        }
        #endregion Public Method
    }
}