﻿using System;

namespace LendFoundry.Whitepages
{
    /// <summary>
    /// Settings class
    /// </summary>
    public class Settings
    {

        /// <summary>
        /// ServiceName
        /// </summary>
        /// <returns></returns>
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "whitepages-pro";

    }
}
