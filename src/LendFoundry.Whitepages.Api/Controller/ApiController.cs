﻿using LendFoundry.Syndication.Whitepages;
using LendFoundry.Syndication.Whitepages.Request;
using LendFoundry.Foundation.Services;
using System.Threading.Tasks;
using LendFoundry.Syndication.Whitepages.Response;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
namespace LendFoundry.Whitepages.Api.Controller
{
    /// <summary>
    ///  ApiController class
    /// </summary>
    [Route("/")]
    public class ApiController : ExtendedController
    {
        #region Public Constructors

        /// <summary>
        /// Constructors
        /// </summary>
        /// <param name="service"></param>
        public ApiController(IWhitepagesService service)
        {
            Service = service;
        }

        #endregion Public Constructors

        #region Private Properties

        private IWhitepagesService Service { get; }

        #endregion Private Properties

        #region Public Methods

        /// <summary>
        /// Verifies the lead.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns>Result for Lead Verification.</returns>
        [HttpPost("{entitytype}/{entityid}/whitepages/verify_lead")]
        [ProducesResponseType(typeof(ILeadVerifyResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> VerifyLead(string entityType, string entityId, [FromBody]LeadVerifyRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(() => Service.VerifyLead(entityType, entityId, request)));
                }
                catch (WhitepagesException exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }

            });
        }

        /// <summary>
        ///  IdentityCheck
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/whitepages/identity_Check")]
        [ProducesResponseType(typeof(IIdentityCheckResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> IdentityCheck(string entityType, string entityId, [FromBody]IdentityCheckRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(() => Service.IdentityCheck(entityType, entityId, request)));
                }
                catch (WhitepagesException exception)
                {
                    return ErrorResult.BadRequest(exception.Message);
                }

            });
        }

        #endregion Public Methods
    }
}