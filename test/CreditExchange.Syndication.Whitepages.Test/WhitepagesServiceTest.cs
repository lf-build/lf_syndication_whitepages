﻿using LendFoundry.Syndication.Whitepages.Proxy;
using LendFoundry.Syndication.Whitepages.Request;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Lookup;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace LendFoundry.Syndication.Whitepages.Test
{
    public class WhitepagesServiceTest
    {
        public WhitepagesServiceTest()
        {
            EventHub = new Mock<IEventHubClient>();
            Lookup = new Mock<ILookupService>();
            Proxy = new WhitepagesProxy(new WhitepagesConfiguration
            {
                //simulation configuration
                ApiBaseUrl = "http://192.168.1.67:7022",
                LeadVerifyApiUrl = "/whitepages/",
                LeadVerifyApiKey = "76f6edf970d547eea83a9f2bead232e6"
            });
            whitepageResponce = new WhitepagesResponseBase();
            whitepageService = new WhitepagesService(Proxy, EventHub.Object, Lookup.Object);
        }

        private Mock<IEventHubClient> EventHub { get; }
        private Mock<ILookupService> Lookup { get; }
        private WhitepagesProxy Proxy { get; }

        private WhitepagesService whitepageService { get; }
        private WhitepagesResponseBase whitepageResponce { get; }

        [Fact]
        public void ArgumentNullException_VerifyLead()
        {
            Dictionary<string, string> categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            Assert.ThrowsAsync<ArgumentNullException>(() => whitepageService.VerifyLead(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<ILeadVerifyRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => whitepageService.VerifyLead("application", It.IsAny<string>(), It.IsAny<ILeadVerifyRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => whitepageService.VerifyLead("application", "12345", It.IsAny<ILeadVerifyRequest>()));
            Assert.ThrowsAsync<ArgumentException>(() => whitepageService.VerifyLead(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<ILeadVerifyRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => whitepageService.VerifyLead(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<ILeadVerifyRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => whitepageService.VerifyLead(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<ILeadVerifyRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => whitepageService.VerifyLead(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<ILeadVerifyRequest>()));
        }

        [Fact]
        public void VerifySuccessWithNotNull_VerifyLead()
        {
            var categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            var VerifyLeadResponse = whitepageService.VerifyLead("application", "CA000001", new Request.LeadVerifyRequest
            {
                Name = "Mc Donalds",
                FirstName = null,
                LastName = null,
                Phone = null,
                Email = null,
                IpAddress = null,
                StreetLine1 = null,
                StreetLine2 = null,
                City = null,
                PostalCode = null,
                StateCode = null,
                CountryCode = null
            }).Result;
            Assert.NotNull(VerifyLeadResponse.PhoneChecks);
        }

        [Fact]
        public void VerifySuccessWithNULL_VerifyLead()
        {
            var categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            var VerifyLeadResponse = whitepageService.VerifyLead("application", "CA000001", new Request.LeadVerifyRequest
            {
                Name = "sss",
                FirstName = null,
                LastName = null,
                Phone = null,
                Email = null,
                IpAddress = null,
                StreetLine1 = null,
                StreetLine2 = null,
                City = null,
                PostalCode = null,
                StateCode = null,
                CountryCode = null
            }).Result;

            Assert.Null(VerifyLeadResponse.PhoneChecks);
        }
    }
}