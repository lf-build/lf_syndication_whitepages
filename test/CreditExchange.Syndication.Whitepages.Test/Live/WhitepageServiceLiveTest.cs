﻿using LendFoundry.Syndication.Whitepages.Proxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace LendFoundry.Syndication.Whitepages.Test.Live
{
    public class WhitepageServiceLiveTest
    {
        private WhitepagesProxy whitepagesProxy { get; }
        private IWhitepagesConfiguration whitepagesConfiguration { get; }

        public WhitepageServiceLiveTest()
        {
            whitepagesConfiguration = new WhitepagesConfiguration
            {
                ApiBaseUrl = "https://proapi.whitepages.com/3.3",
                LeadVerifyApiKey = "76f6edf970d547eea83a9f2bead232e6",
                LeadVerifyApiUrl = "/lead_verify.json"
            };
            whitepagesProxy = new WhitepagesProxy(whitepagesConfiguration);
        }

        /// <summary>
        /// Verifies the lead response.
        /// </summary>
        [Fact]
        public void VerifyLead_Response()
        {
            var response = whitepagesProxy.VerifyLead(new Proxy.Request
            {
                Name = "Drama Number",
                FirstName = "",
                LastName = "",
                Phone = "6464806649",
                Email = "medjalloh1@yahoo.com",
                IpAddress = "108.194.128.165",
                StreetLine1 = "302 Gorham Ave",
                StreetLine2 = "",
                City = "Ashland",
                PostalCode = "59004",
                StateCode = "MT",
                CountryCode = ""
            });
            Assert.NotNull(response);
        }
    }
}