﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Syndication.Whitepages.Proxy;
using Xunit;

namespace LendFoundry.Syndication.Whitepages.Test
{
    public class WhitepagesConfigurationTest
    {
        [Fact]
        public void ThrowArgumentExceptionWithNullConfiguration()
        {
            Assert.Throws<ArgumentNullException>(() => new WhitepagesProxy(null));
        }

        [Fact]
        public void ThrowArgumentExceptionWithMissingConfiguration()
        {
            var whitepageConfiguration = new WhitepagesConfiguration
            {
                ApiBaseUrl = "",
                LeadVerifyApiKey = "",
                LeadVerifyApiUrl = ""
            };

            Assert.Throws<ArgumentNullException>(() => new WhitepagesProxy(whitepageConfiguration));
            var whitepageConfiguration1 = new WhitepagesConfiguration
            {
                ApiBaseUrl = "www.sigma.net",
                LeadVerifyApiKey = "",
                LeadVerifyApiUrl = ""
            };

            Assert.Throws<ArgumentNullException>(() => new WhitepagesProxy(whitepageConfiguration1));
            var whitepageConfiguration2 = new WhitepagesConfiguration
            {
                ApiBaseUrl = "",
                LeadVerifyApiKey = "12345",
                LeadVerifyApiUrl = "www.test.com"
            };

            Assert.Throws<ArgumentNullException>(() => new WhitepagesProxy(whitepageConfiguration2));
        }
    }
}