﻿using LendFoundry.Syndication.Whitepages;
using LendFoundry.Syndication.Whitepages.Request;
using LendFoundry.Syndication.Whitepages.Response;
using LendFoundry.Foundation.Services;
using Moq;
using RestSharp;
using Xunit;

namespace LendFoundry.Whitepages.Client.Test
{
    public class WhitepagesServiceClientTest
    {
        private Mock<IServiceClient> Client { get; set; }
        public WhitepagesService whitepageServiceClient { get; }

        private IRestRequest restRequest { get; set; }

        public WhitepagesServiceClientTest()
        {
            Client = new Mock<IServiceClient>();
            whitepageServiceClient = new WhitepagesService(Client.Object);
        }

        [Fact]
        public async void client_VerifyLead()
        {
            Syndication.Whitepages.Proxy.LeadVerifyResponse whitepageResponse = new Syndication.Whitepages.Proxy.LeadVerifyResponse();
            whitepageResponse.AddressChecks = null;
            whitepageResponse.EmailAddressChecks = null;
            whitepageResponse.IpAddressChecks = null;
            whitepageResponse.PhoneChecks = null;

            Client.Setup(s => s.ExecuteAsync<LeadVerifyResponse>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => restRequest = r).ReturnsAsync(new LeadVerifyResponse(whitepageResponse));

            var response = await whitepageServiceClient.VerifyLead("application", "test", It.IsAny<ILeadVerifyRequest>());
            Assert.Equal("{entitytype}/{entityid}/whitepages/verify_lead", restRequest.Resource);
            Assert.Equal(Method.POST, restRequest.Method);
        }
    }
}