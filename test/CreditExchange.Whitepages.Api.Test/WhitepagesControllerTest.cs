﻿using LendFoundry.Syndication.Whitepages;
using LendFoundry.Syndication.Whitepages.Request;
using LendFoundry.Syndication.Whitepages.Response;
using LendFoundry.Whitepages.Api.Controller;
using LendFoundry.Foundation.Services;
using Microsoft.AspNet.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace LendFoundry.Whitepages.Api.Test
{
    public class WhitepagesControllerTest
    {
        private Mock<IWhitepagesService> whitepageService { get; }

        private ApiController GetController(IWhitepagesService whitepageService)
        {
            return new ApiController(whitepageService);
        }

        private Mock<IWhitepagesService> GetwhitepageService()
        {
            return new Mock<IWhitepagesService>();
        }

        [Fact]
        public async void VerifyLeadReturnOnSucess()
        {
            var whitepageService = GetwhitepageService();
            whitepageService.Setup(x => x.VerifyLead("", "", It.IsAny<LeadVerifyRequest>())).ReturnsAsync(new LeadVerifyResponse());
            var response = (HttpOkObjectResult)await GetController(whitepageService.Object).VerifyLead("application", "123", It.IsAny<LeadVerifyRequest>());
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 200);
        }

        [Fact]
        public async void VerifyLeadReturnOnError()
        {
            var whitepageService = GetwhitepageService();
            whitepageService.Setup(x => x.VerifyLead(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<LeadVerifyRequest>())).Throws(new WhitepagesException());
            var response = (ErrorResult)await GetController(whitepageService.Object).VerifyLead("application", "123", It.IsAny<LeadVerifyRequest>());
            Assert.NotNull(response);
            Assert.Equal(response.StatusCode, 400);
        }
    }
}